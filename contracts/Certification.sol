pragma solidity ^0.5.0;

contract Certification {
    
    struct Certificate {
        string password;
        string name;
        string schoolName;
        string studentName;
        address studentAddress;
        address schoolAddress;
        uint grade;
        string imageLink;
        
    }
    
    uint public numberOfCertificates = 0;
    
    mapping(string => Certificate) private certifications;
    
    function createCertification(string memory _password, string memory _name, string memory _schoolName, string memory _studentName, address _studentAddress, uint  _grade, string memory _imageLink) public {
        
        Certificate memory _certificate = Certificate(_password, _name, _schoolName, _studentName, _studentAddress, msg.sender, _grade, _imageLink);
        certifications[_password] = _certificate;
        numberOfCertificates ++;
    }
    
    function getCertificateName(string memory _password) public view returns(string memory){
        return certifications[_password].name;
    }
    
    function getCertificateSchoolName(string memory _password) public view returns(string memory){
        return certifications[_password].schoolName;
    }
    
    function getCertificateSchoolAddress(string memory _password) public view returns(address){
        return certifications[_password].schoolAddress;
    }
    
    function getCertificateStudentAddress(string memory _password) public view returns(address){
        return certifications[_password].studentAddress;
    }
    
    function getCertificateStudentName(string memory _password) public view returns(string memory){
        return certifications[_password].studentName;
    }
    
    function getCertificateGrade(string memory _password) public view returns(uint){
        return certifications[_password].grade;
    }
    
    function getCertificateImageLink(string memory _password) public view returns(string memory){
        return certifications[_password].imageLink;
    }
}