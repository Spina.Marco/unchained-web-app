var HDWalletProvider = require("truffle-hdwallet-provider");
const privateKey = '9066d998ade37c0ddf3cf22f57f5fc43010dac8e949698ba6a97e71c5dc5a09a';


module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    ropsten: {
      provider: function() {
        return new HDWalletProvider(privateKey, "https://ropsten.infura.io/v3/77fd8c86fd8942b69eaa3a76db7c49fb")
      },
      network_id: 3,
      gas: 4000000      //make sure this gas allocation isn't over 4M, which is the max
    }
  }
};
