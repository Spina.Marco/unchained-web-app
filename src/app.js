App = {

	contracts: {},

	load: async () => {
		await App.loadWeb3()
		await App.loadAccount()
		await App.loadContract()
		await App.render()
		//await App.createTask()
	},

// https://medium.com/metamask/https-medium-com-metamask-breaking-change-injecting-web3-7722797916a8
  loadWeb3: async () => {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider
      web3 = new Web3(web3.currentProvider)
    } else {
      window.alert("Please connect to Metamask.")
    }
    // Modern dapp browsers...
    if (window.ethereum) {
      window.web3 = new Web3(ethereum)
      try {
        // Request account access if needed
        await ethereum.enable()
        // Acccounts now exposed
        web3.eth.sendTransaction({/* ... */})
      } catch (error) {
        // User denied account access...
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      App.web3Provider = web3.currentProvider
      window.web3 = new Web3(web3.currentProvider)
      // Acccounts always exposed
      web3.eth.sendTransaction({/* ... */})
    }
    // Non-dapp browsers...
    else {
      console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  },

  loadAccount: async () => {
  	App.account = web3.eth.accounts[0]
  	console.log(App.account)
  },

  loadContract: async () => {
  	const certification = await $.getJSON('Certification.json')
  	App.contracts.Certification = TruffleContract(certification)
  	App.contracts.Certification.setProvider(App.web3Provider)
  	
  	App.certification = await App.contracts.Certification.deployed()
  },

  render: async () => {
  	$('#account').html(App.account)
  },

  createTask: async () => {
  	const certificationName = $('#certificationName').val()
  	const schoolName = $('#schoolName').val()
  	const studentName = $('#studentName').val()
  	const studentAddress = $('#studentAddress').val()
  	const grade = $('#grade').val()
  	const imageLink = $('#certificationLink').val()
  	const password = $('#password').val()

  	await App.certification.createCertification(password, certificationName, schoolName, studentName, studentAddress, grade, imageLink)

  	//await App.certification.createCertification("1234", "aaa", "schoolName", "studentName", "0x27eFc7fE135521098ac5159E419c213Ee74a3389", 60, "test")

  },

}

$(() => {
  $(window).load(() => {
    App.load()
  })
})
